function crearEscala(){
    
    var numero = document.getElementById("entrada").value;
    var rta = crearOperacion(0,numero);
    var rta2 = crearOperacion(1,rta[0]);

    var tabla = "<table class='center'>";
    tabla += "<tr>" + "<th colspan='3'> ESCALA DEL NÚMERO : " + numero + "</th></tr><tr><th colspan='3'> Operación multiplicar </th></tr><tr><th> Dato 1 </th><th> Dato 2 </th><th> Resultado </th></tr>";
    tabla += rta[1] + "<tr><th colspan='3'> Operación dividir </th></tr>" + rta2[1];
    tabla += "</table>";

    document.getElementById("rta3").innerHTML = tabla;
} 

function crearOperacion(operacion,numero){
    let m = ["*", "/"];
    let vectorRta = new Array(2);
    var rta = "";
    
    for(var i = 1; i <= 10; i++){
      rta += "<tr><td>" + numero + "</td><td>" + i + "</td>";
      operacion == 0 ? numero *= i : numero /= i;
      rta += "<td>" + numero + "</td></tr>";
    }

    vectorRta[0] = numero;
    vectorRta[1] = rta;
    return vectorRta;
}